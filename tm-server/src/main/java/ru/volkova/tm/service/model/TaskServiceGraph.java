package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.model.ITaskServiceGraph;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.model.TaskGraph;
import ru.volkova.tm.repository.model.TaskRepositoryGraphGraph;
import ru.volkova.tm.repository.model.UserRepositoryGraphGraph;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskServiceGraph extends AbstractServiceGraph<TaskGraph> implements ITaskServiceGraph {

    public TaskServiceGraph(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public TaskGraph insert(@NotNull final TaskGraph taskGraph) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.insert(taskGraph);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull TaskGraph taskGraph = new TaskGraph();
        taskGraph.setName(name);
        taskGraph.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final UserRepositoryGraphGraph userRepository = new UserRepositoryGraphGraph(entityManager);
            taskGraph.setUserGraph(userRepository.findById(userId));
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.insert(taskGraph);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<TaskGraph> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            entities.forEach(taskRepository::insert);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    public List<TaskGraph> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            final List<TaskGraph> taskGraphs = taskRepository.findAll(userId);
            entityManager.getTransaction().commit();
            return taskGraphs;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public TaskGraph findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            final TaskGraph taskGraph = taskRepository.findById(userId, id);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public TaskGraph findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            final TaskGraph taskGraph = taskRepository.findOneByIndex(userId, index);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskGraph findOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            final TaskGraph taskGraph = taskRepository.findOneByName(userId, name);
            entityManager.getTransaction().commit();
            return taskGraph;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeOneStatusById(
            @NotNull String userId, @NotNull String id, @Nullable Status status
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.changeOneStatusById(userId, id, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void changeOneStatusByName(
            @NotNull String userId, @NotNull String name, @Nullable Status status
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.changeOneStatusByName(userId, name, status);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.removeOneByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final TaskRepositoryGraphGraph taskRepository = new TaskRepositoryGraphGraph(entityManager) ;
            taskRepository.updateOneById(userId, id, name, description);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
