package ru.volkova.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.dto.*;
import ru.volkova.tm.model.ProjectGraph;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.model.SessionGraph;
import ru.volkova.tm.model.TaskGraph;
import ru.volkova.tm.model.UserGraph;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = factory();
    }

    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getJdbcDriver());
        settings.put(Environment.URL, propertyService.getJdbcUrl());
        settings.put(Environment.USER, propertyService.getJdbcUser());
        settings.put(Environment.PASS, propertyService.getJdbcPassword());
        settings.put(Environment.DIALECT, propertyService.getDialect());
        settings.put(Environment.HBM2DDL_AUTO,propertyService.getHbm2dllAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getUseSecondLevelCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProviderConfig());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactory());
        settings.put(PropertyService.USE_LITE_MEMBER, propertyService.getUseLiteMemberValue());

        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(ProjectGraph.class);

        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(TaskGraph.class);

        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(SessionGraph.class);

        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(UserGraph.class);

        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
