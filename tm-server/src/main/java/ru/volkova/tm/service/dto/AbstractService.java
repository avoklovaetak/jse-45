package ru.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.dto.IService;
import ru.volkova.tm.dto.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
