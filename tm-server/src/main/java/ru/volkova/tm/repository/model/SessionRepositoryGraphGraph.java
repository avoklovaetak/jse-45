package ru.volkova.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.model.ISessionRepositoryGraph;
import ru.volkova.tm.model.SessionGraph;

import javax.persistence.EntityManager;

public class SessionRepositoryGraphGraph extends AbstractRepositoryGraphGraph<SessionGraph> implements ISessionRepositoryGraph {

    public SessionRepositoryGraphGraph(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull SessionGraph sessionGraph) {
        entityManager.persist(sessionGraph);
    }

    @Override
    public void close(@NotNull SessionGraph sessionGraph) {
        entityManager.createQuery("DELETE t FROM session t", SessionGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

}
