package ru.volkova.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.dto.ISessionRepository;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.model.SessionGraph;

import javax.persistence.EntityManager;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void add(@NotNull Session session) {
        entityManager.persist(session);
    }

    @Override
    public void close(@NotNull Session session) {
        entityManager.createQuery("DELETE t FROM session t", SessionGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

}
