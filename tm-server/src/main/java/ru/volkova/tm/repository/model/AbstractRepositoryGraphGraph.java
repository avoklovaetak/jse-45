package ru.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.model.AbstractEntityGraph;

import javax.persistence.EntityManager;

public abstract class AbstractRepositoryGraphGraph<E extends AbstractEntityGraph> implements IRepositoryGraph<E> {

    protected final EntityManager entityManager;

    protected AbstractRepositoryGraphGraph(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
