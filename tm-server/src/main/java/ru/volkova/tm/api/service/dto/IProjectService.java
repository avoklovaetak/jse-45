package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface IProjectService extends IService<Project> {

    void add(
            @NotNull String userId,
            @Nullable String name,
            @Nullable String description
    );

    void insert(@Nullable final Project project);

    void addAll(@Nullable List<Project> entities);

    void clear(@NotNull String userId);

    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@NotNull String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
