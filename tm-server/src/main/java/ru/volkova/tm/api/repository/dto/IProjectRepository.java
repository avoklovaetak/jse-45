package ru.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    Project insert(@Nullable final Project project);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@Nullable String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
