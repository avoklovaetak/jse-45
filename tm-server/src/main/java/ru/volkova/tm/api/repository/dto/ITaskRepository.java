package ru.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    Task insert(@Nullable final Task task);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId,@NotNull String projectId);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @Nullable
    Task findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@NotNull String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
