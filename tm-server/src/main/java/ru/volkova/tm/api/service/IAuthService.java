package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;

public interface IAuthService {

    void checkRoles(@Nullable Role... roles);

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
