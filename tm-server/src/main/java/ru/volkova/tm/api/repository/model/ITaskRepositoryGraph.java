package ru.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.model.TaskGraph;

import java.util.List;

public interface ITaskRepositoryGraph extends IRepositoryGraph<TaskGraph> {

    @Nullable
    TaskGraph insert(@Nullable final TaskGraph taskGraph);

    void clear(@NotNull String userId);

    @NotNull
    List<TaskGraph> findAll(@NotNull String userId);

    void bindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @NotNull
    List<TaskGraph> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAllByProjectId(@NotNull String userId,@NotNull String projectId);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @NotNull String projectId,
            @NotNull String taskId
    );

    @Nullable
    TaskGraph findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    TaskGraph findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskGraph findOneByName(@NotNull String userId, @NotNull String name);

    void changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByName(@NotNull String userId,@NotNull String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
