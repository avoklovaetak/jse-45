package ru.volkova.tm.constant;

import org.jetbrains.annotations.NotNull;

public class ArgumentConst {

    @NotNull
    public static final String ARG_VERSION = "-v";

    @NotNull
    public static final String ARG_ABOUT = "-a";

    @NotNull
    public static final String ARG_HELP = "-h";

    @NotNull
    public static final String ARG_INFO = "-i";

}
