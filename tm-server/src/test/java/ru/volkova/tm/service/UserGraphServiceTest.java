package ru.volkova.tm.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.api.service.dto.IUserService;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.marker.UnitCategory;
import ru.volkova.tm.service.dto.AdminUserService;
import ru.volkova.tm.service.dto.UserService;

public class UserGraphServiceTest {

    private final IPropertyService propertyService = new PropertyService();

    private final IConnectionService connectionService = new ConnectionService(propertyService);

    private final IAdminUserService adminUserService = new AdminUserService(connectionService, propertyService);

    private final IUserService userService = new UserService(connectionService);

    @Test
    @Category(UnitCategory.class)
    public void addTest() {
        final User user = new User();
        Assert.assertNotNull(adminUserService.add(user));
    }

}
