package ru.volkova.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.volkova.tm.api.service.EndpointLocator;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.marker.IntegrationCategory;

public class SessionEndpointTest {

    private final EndpointLocator endpointLocator = new Bootstrap();

    @Test
    @Category(IntegrationCategory.class)
    public void openSessionTest() {
        final Session session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
        Assert.assertNotNull(session);
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @Test(expected = ServerSOAPFaultException.class)
    @Category(IntegrationCategory.class)
    public void closeSession() {
        final Session session = endpointLocator.getSessionEndpoint().openSession("admin", "pass");
        endpointLocator.getSessionEndpoint().closeSession(session);
        endpointLocator.getAdminEndpoint().dataBase64Load(session);
    }

}
